#!/usr/bin/python3

__author__ = "Bart Nijsse and Jasper Koehorst"
__copyright__ = "Copyright 2021, UNLOCK"
__credits__ = ["Bart Nijsse", "Jasper Koehorst"]
__license__ = "CC0"
__version__ = "1.0.0"
__status__ = "Development"

import json
from irods.session import iRODSSession
import ssl
import os
import argparse
import pandas as pd
import rdflib
import irods.keywords as kw

host = os.getenv('irodsHost')
port = os.getenv('irodsPort')
zone = os.getenv('irodsZone')
user = os.getenv('irodsUserName')
password = os.getenv('irodsPassword')

options = {
kw.FORCE_FLAG_KW: True,
}


# SSL settings
context = ssl.create_default_context(purpose=ssl.Purpose.SERVER_AUTH, cafile=None, capath=None, cadata=None)

ssl_settings = {
    "irods_client_server_negotiation": "request_server_negotiation",
    "irods_client_server_policy": "CS_NEG_REQUIRE",
    "irods_encryption_algorithm": "AES-256-CBC",
    "irods_encryption_key_size": 32,
    "irods_encryption_num_hash_rounds": 16,
    "irods_encryption_salt_size": 8,
    "irods_home": "/unlock/home",
    "irods_ssl_verify_server": "none",
    "irods_zone_name": "unlock",
    'ssl_context': context
    }

def sumall(file_or_directory):
    # Connect with irods
    with iRODSSession(
        host = host,
        port = port,
        user = user,
        password = password,
        zone = zone,
        **ssl_settings) as session:
        if file_or_directory.endswith(".ttl"):
            rdf_file = file_or_directory

            if not os.path.isfile(rdf_file):
                path = os.path.dirname(rdf_file)
                if not os.path.isdir(path):
                    os.makedirs(path)
                print("Downloading %s" % rdf_file)
                session.data_objects.get(rdf_file, path, **options)
        else:
            directory = file_or_directory
            if not os.path.isdir(directory):
                os.makedirs(directory)
                print("Downloading directory %s" % directory)
                for data_object in session.collections.get(directory).data_objects:
                    session.data_objects.get(data_object.path, directory, **options)

    return "done"

# Obtain the data for the biom files
def process_job_file():
    # Connect with irods
    with iRODSSession(
        host = host,
        port = port,
        user = user,
        password = password,
        zone = zone,
        **ssl_settings) as session:

        # For line in the job file
        data_folders = set()
        rdf_files = set()

        lines = open(args.job).readlines()
        for index, line in enumerate(lines):
            line = line.strip() # .decode("UTF-8").strip()
            # print("Processing", index,"of",len(lines), line)
            # Old version of job file had ttl project file but with latest workflow not needed
            if line.endswith(".ttl"):
                rdf_files.add(line)
            else:
                data_folders.add(line)
    
        import multiprocessing
        pool_obj = multiprocessing.Pool(10)
        pool_obj.map(sumall, data_folders)
        pool_obj.map(sumall, rdf_files)
        print("Finished downloading")

        # Obtain the job file
        # job_file = session.data_objects.get(args.job)
        destinations = {
            "seq.tsv" : open("seq.tsv", "w"), 
            "asv.tsv" : open("asv.tsv", "w"),
            "tax.tsv" : open("tax.tsv", "w"),
            "met.tsv" : open("met.tsv", "w")
        }

        for index, folder in enumerate(data_folders):
            print("Processing ", index, end="\r")
            for data_file in os.listdir(folder):
                id = data_file.split("_")[-1]
                path = folder + "/" + data_file
                content = open(path).read()
                print(content, file=destinations[id])
        for destination in destinations:
            destinations[destination].close()




        #     # List the files in this folder using the walk function
        #     walk = session.collections.get(line).walk()
        #     # Just loop over the files as they are irods objects
        #     for root, dirs, files in walk:
        #         # For each file in this directory / sub directories?
        #         for file in files:
        #             if os.path.exists("./irods/" + file.name): continue
        #             # Extra check if there is no accidental file created in there e.g. .DS_Store
        #             if file.path.split("_")[-1] in destinations:
        #                 # Download the file
        #                 session.data_objects.get(file.path, "./irods/" + file.name)
        
        # for file_name in os.listdir("./irods"):    
        #     # Obtain output writer
        #     output = destinations[file_name.split("_")[-1]]
        #     for line_file in open("./irods/" + file_name):
        #         line_file = line_file.strip()
        #         print(line_file, file=output)

        # # Close and flush all writers
        # for writer in destinations:
        #     destinations[writer].close()

def biom_preformatter():
    # Format tax matrix a bit so further parsing as added prefixes
    tax_df = process_tax()
    
    # Load and transform the ASV file into a matrix
    lines = []
    size = {"nRow": set(), "nCol": set()}
    for line in open("asv.tsv"):
        line = line.strip().split("\t")
        if len(line) == 1: continue
        lines.append(line)
        # Row sample
        size["nRow"].add(line[1])
        # Column ASV
        size["nCol"].add(line[0])
    
    # Three columns to matrix method
    # df = pd.DataFrame(lines, columns=['Y','X','Z'])
    # df = df.pivot(index='X', columns='Y', values='Z')
    
    df = pd.DataFrame(index=range(len(size["nRow"])),columns=range(len(size["nCol"])))
    df.columns = size["nCol"]
    
    asv_index = sorted(list(size["nRow"]))
    df.index = asv_index

    for index, line in enumerate(lines):
        if index % 10000 == 0:
            print(index, len(lines), end="\r")
        sample, asv, value = line
        df.loc[asv, sample]=value

    # Merge with taxon labels
    result = pd.merge(df, tax_df, left_index=True, right_index=True)
    result = result.drop_duplicates(keep="first")
    result.fillna(0, inplace=True)
    
    # Rename the rownames
    row_names = result.index.values
    for index, value in enumerate(row_names):
        row_names[index] = "ASV_" + str(index + 1)
    result.index = row_names
    
    # Write to file
    result.to_csv("merged.tsv", sep="\t")

    # Load and fix first line
    lines = open("merged.tsv").readlines()
    lines.insert(0, "# Automatically generated input file for biom conversion")
    lines[1] = "#OTU ID" + lines[1]
    output = open("merged.tsv", "w")
    for line in lines:
        print(line.strip(), file=output)
    
# def process_met():
#     # Load and transform the ASV file into a matrix
#     lines = []
#     for line in open("met.tsv"):
#         line = line.strip().split("\t")
#         lines.append(line)
    
#     # Three columns to matrix method
#     df = pd.DataFrame(lines, columns=['Y','X','Z'])
#     df = df.pivot(index='X', columns='Y', values='Z')
#     df.fillna("None", inplace=True)
#     df.to_csv("metadata.tsv", sep="\t")
#     print(df)

def tsv_to_biom():
    from biom import load_table
    from collections import OrderedDict

    # merged.tsv
    biom_content = load_table("merged.tsv")
    
    # Load RDF metadata file
    sample_ids = set(biom_content._sample_ids)
    metadata = {}
    keys = set()
    for line in open("met.tsv"):
        line = line.strip()
        if "\t" not in line: continue
        identifier, key, value = line.split("\t")
        # Bug in biom reader, all metadata need to have the same keys in the same order
        keys.add(key)
        # Skip lines that are not in this biom object
        if identifier not in sample_ids: continue
        if identifier not in metadata:
            metadata[identifier] = {}
        metadata[identifier][key] = value
        # print(metadata)
    
    keys = sorted(keys)
    for identifier in metadata:
        for key in keys:
            if key not in metadata[identifier]:
                metadata[identifier][key] = "None"

    # Add metadata
    biom_content.add_metadata(metadata)
    biom_content.type = "OTU table"
    json_data = biom_content.to_json(generated_by="UNLOCK conversion module")
    
    # Create Python object from JSON string data
    obj = json.loads(json_data)
    
    # Fix taxonomy split issue
    for index, row in enumerate(obj["rows"]):
        row['metadata']['taxonomy'] = row['metadata']['taxonomy'].split("; ")
    
    # Pretty Print JSON
    json_formatted_str = json.dumps(obj, indent=4, sort_keys=True)
    
    # Create biom file
    biom_file = args.identifier + "_" + job.split("/")[-1].replace(".job", ".biom")
    print("Writing biom file to", biom_file)
    print(json_formatted_str, file=open(biom_file, "w"))


def process_tax():
    lines = []
    for line in open("tax.tsv"):
        line = line.strip().split("\t")
        if len(line) < 8:
            line = line[:8] + ["NA"]*(8 - len(line))
        formatted = [line[0], "k__" + line[1]+ "; p__" + line[2]+ "; c__" + line[3]+ "; o__" + line[4]+ "; f__" + line[5]+ "; g__" + line[6]+ "; s__" + line[7]]
        lines.append(formatted)
    df = pd.DataFrame(lines)
    df.columns = ["ID", "taxonomy"]
    df = df.set_index("ID")
    return df


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description='Biom file creation')
    parser.add_argument('-j', '--job', help='input Job file', required=True)
    parser.add_argument('-i', '--identifier', help='Prefix identifier', required=True)

    args = parser.parse_args()

    job = args.job

    # Process the job file to create a biom json file
    process_job_file()
    biom_preformatter()
    tsv_to_biom()