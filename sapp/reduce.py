from irods.session import iRODSSession
import ssl
from irods.meta import iRODSMeta
import os, sys
from irods.column import Criterion
from irods.models import DataObject, DataObjectMeta, Collection, CollectionMeta
import requests

# iRODS authentication information
host = "unlock-icat.irods.surfsara.nl"
port = 1247
zone = "unlock"
user = os.getenv('irodsUserName')
password = os.getenv('irodsPassword')

# SSL settings
context = ssl.create_default_context(purpose=ssl.Purpose.SERVER_AUTH, cafile=None, capath=None, cadata=None)

ssl_settings = {'irods_client_server_negotiation': 'request_server_negotiation',
                'irods_client_server_policy': 'CS_NEG_REQUIRE',
                'irods_encryption_algorithm': 'AES-256-CBC',
                'irods_encryption_key_size': 32,
                'irods_encryption_num_hash_rounds': 16,
                'irods_encryption_salt_size': 8,
                'ssl_context': context}

# Authentication
with iRODSSession(
    host = host,
    port = port,
    user = user,
    password = password,
    zone = zone,
    **ssl_settings) as session:
    genome_collection = session.collections.get("/unlock/references/genomes")
    for col in genome_collection.subcollections:
        results = session.query(Collection, CollectionMeta).filter( \
            Criterion('like', Collection.name, col.path + '%')).filter( \
            Criterion('=', CollectionMeta.name, 'lineage')).filter( \
            Criterion('like', CollectionMeta.value, 'bacteria%'))
        for result in results:
            genome_folder = session.collections.get(result[Collection.name])
            for data in genome_folder.data_objects:
                path = data.path
                if path.endswith(".hdt.gz"):
                    dest = '.' + path
                    dirname, filename = os.path.split(dest)
                    os.makedirs(dirname, exist_ok=True)
                    if not os.path.isfile(dest):
                        session.data_objects.get(path, dest)
                    if not os.path.isfile(dest + ".ttl"):
                        # ASK SAPP TO REDUCE from http://download.systemsbiology.nl/sapp/SAPP-2.0.jar
                        command = "java -jar SAPP-2.0.jar -reduce -i " + dest + " -o " + dest + ".ttl"
                        os.system(command)
