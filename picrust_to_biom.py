#!/usr/bin/python3

__author__ = "Bart Nijsse and Jasper Koehorst"
__copyright__ = "Copyright 2022, UNLOCK"
__credits__ = ["Bart Nijsse", "Jasper Koehorst"]
__license__ = "CC0"
__version__ = "1.0.0"
__status__ = "Development"

import argparse
import gzip
import json
from irods.session import iRODSSession
import ssl
import os
import argparse
import pandas as pd
from rdflib.namespace import Namespace
from rdflib import Graph
import irods.keywords as kw
from rdflib.term import Literal
from biom import load_table
from collections import OrderedDict

host = os.getenv('irodsHost')
port = os.getenv('irodsPort')
zone = os.getenv('irodsZone')
user = os.getenv('irodsUserName')
password = os.getenv('irodsPassword')

options = {
kw.FORCE_FLAG_KW: True,
}


# SSL settings
context = ssl.create_default_context(purpose=ssl.Purpose.SERVER_AUTH, cafile=None, capath=None, cadata=None)

ssl_settings = {
    "irods_client_server_negotiation": "request_server_negotiation",
    "irods_client_server_policy": "CS_NEG_REQUIRE",
    "irods_encryption_algorithm": "AES-256-CBC",
    "irods_encryption_key_size": 32,
    "irods_encryption_num_hash_rounds": 16,
    "irods_encryption_salt_size": 8,
    "irods_home": "/unlock/home",
    "irods_ssl_verify_server": "none",
    "irods_zone_name": "unlock",
    'ssl_context': context
    }

def sumall(file):
    # Connect with irods
    with iRODSSession(
        host = host,
        port = port,
        user = user,
        password = password,
        zone = zone,
        **ssl_settings) as session:

        if not os.path.isfile(file):
            path = os.path.dirname(file)
            if not os.path.isdir(path):
                os.makedirs(path)
            
            try:
                session.data_objects.get(file, path, **options)
                print("Download success for %s" % file)
            except:
                print("Download failed for %s" % file)

    return "done"


# Obtain the data for the biom files
def process_job_file():
    # Connect with irods
    with iRODSSession(
        host = host,
        port = port,
        user = user,
        password = password,
        zone = zone,
        **ssl_settings) as session:

        # Obtain the job file
        # job_file = session.data_objects.get(args.job)
        file_names = ["combined_COG_pred_metagenome_unstrat.tsv", "combined_EC_pred_metagenome_unstrat.tsv", "combined_KO_pred_metagenome_unstrat.tsv", "combined_PFAM_pred_metagenome_unstrat.tsv", "combined_TIGRFAM_pred_metagenome_unstrat.tsv", "combined_path_abun_unstrat.tsv"]
        
        writers = []
        for index, element in enumerate(file_names):
            writer = open(file_names[index], "w")
            writers.append(writer)
        
        # For line in the job file
        lines = open(args.job).readlines()
        files = set()
        rdf_files = set()
        for index, line in enumerate(lines):
            line = line.strip()
            # Metadata file
            if line.endswith(".ttl"):
                rdf_files.add(line)
            else:
                # Get the files directly
                files.add(line + "/COG_metagenome_out/pred_metagenome_unstrat.tsv.gz")
                files.add(line + "/EC_metagenome_out/pred_metagenome_unstrat.tsv.gz")
                files.add(line + "/KO_metagenome_out/pred_metagenome_unstrat.tsv.gz")
                files.add(line + "/PFAM_metagenome_out/pred_metagenome_unstrat.tsv.gz")
                files.add(line + "/TIGRFAM_metagenome_out/pred_metagenome_unstrat.tsv.gz")
                files.add(line + "/pathways_out/path_abun_unstrat.tsv.gz")

        file_names = [
            "combined_COG_pred_metagenome_unstrat.tsv", 
            "combined_EC_pred_metagenome_unstrat.tsv", 
            "combined_KO_pred_metagenome_unstrat.tsv", 
            "combined_PFAM_pred_metagenome_unstrat.tsv", 
            "combined_TIGRFAM_pred_metagenome_unstrat.tsv", 
            "combined_path_abun_unstrat.tsv"
        ]

        writers = []
        for index, element in enumerate(file_names):
            writer = open(file_names[index], "w")
            writers.append(writer)

        # Prepare for downloading
        import multiprocessing
        pool_obj = multiprocessing.Pool(10)
        pool_obj.map(sumall, files)
        print("Finished downloading")
        # Combine into single files per file type
        for index, file_path in enumerate(files):
            print("Processing", index, end="\r")
            if "/ASY_" in file_path:
                sample_id = file_path.split("/ASY_")[-1].split("/")[0]
            else:
                print("Unknown file path %s" % file_path)

            if "COG_metagenome_out" in file_path:
                output = writers[0]
            if "EC_metagenome_out" in file_path:
                output = writers[1]
            if "KO_metagenome_out" in file_path:
                output = writers[2]
            if "PFAM_metagenome_out" in file_path:
                output = writers[3]
            if "TIGRFAM_metagenome_out" in file_path:
                output = writers[4]
            if "pathways_out" in file_path:
                output = writers[5]

            content = gzip.open(file_path, mode='r').readlines()

            for line_file in content:
                line_file = line_file.decode("UTF-8").strip()
                # Skip function line
                if "function" in line_file: continue
                line = sample_id + "\t" + line_file
                print(line, file=output)
        
        # Close all writes
        for writer in writers:
            writer.close()

        # Three columns to matrix method
        print("Converting dataset to matrix")
        for file_name in file_names:
            content = []
            with open(file_name) as reader:
                for line in reader:
                    line = line.strip().split()
                    content.append(line)
            if len(content) == 0: 
                print("No content for", file_name)
                continue
            df = pd.DataFrame(content, columns=['Y','X','Z'])
            df = df.pivot(index='X', columns='Y', values='Z')
            df.to_csv(file_name, sep="\t", index_label=False )
    
    # Process RDF files
    print("Processing RDF metadata files")
    for rdf_file in rdf_files:
        print(rdf_file)
        if not os.path.isfile(rdf_file):
            path = os.path.dirname(rdf_file)
            if not os.path.isdir(path):
                os.makedirs(path)
            print("Downloading %s" % rdf_file)
            session.data_objects.get(rdf_file, path, **options)
        process_rdf_files(rdf_file)
    
    metadata_file = "metadata.picrust.tsv"
    
    remove_duplicates(metadata_file)

    content = []
    with open(metadata_file) as reader:
        lines = set(reader.readlines())
        for line in lines:
            line = line.strip().split("\t")
            if len(line) > 3:
                print(line)
            content.append(line)
    df = pd.DataFrame(content, columns=['Y','X','Z'])
    df = df.pivot(index='X', columns='Y', values='Z')
    df = df.fillna("None")
    df.to_csv(metadata_file, sep="\t", index_label=False)

    # Create biom files
    for file_name in file_names:
        tsv_to_biom(file_name)
    
def remove_duplicates(input_file):
    print("Removing duplicates")
    # Remove duplicates due to multiple TTL files, work in progress to prevent in the future
    lines = open(input_file).readlines()
    keys = {}
    for index, line in enumerate(lines):
        key1, key2, value = line.strip().split("\t")
        # If key1 not avilable create
        if key1 not in keys:
            keys[key1] = {}
        # If key2 in key set we have a duplication issue
        if key2 not in keys[key1]:
            keys[key1][key2] = set()
        keys[key1][key2].add(value)
    
    output = open(input_file, "w")
    for key1 in keys:
        for key2 in keys[key1]:
            if len(keys[key1][key2]) > 1:
                print("Duplication detected", key1, key2, keys[key1][key2])
                keys[key1][key2] = set(["MULTI VALUE"])
            for value in keys[key1][key2]:
                print(key1, key2, value, sep="\t", file=output)
    output.close()

def process_rdf_files(rdf_file):
    print("Processing rdf file", rdf_file)
    g = Graph()
    g.bind("unlock", Namespace("http://m-unlock.nl/ontology/"))
    g.bind("schema", Namespace("http://schema.org/"))
    g.parse(rdf_file, format="turtle")
    output = open("metadata.picrust.tsv", "a")
    qres = g.query("""
    PREFIX gbol:<http://gbol.life/0.1/>
    PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
    PREFIX ssb: <http://ssb.wur.nl/>
    PREFIX ns1: <http://ssb.wur.nl/model/>
    PREFIX unlock: <http://m-unlock.nl/ontology/>
    PREFIX jerm: <http://jermontology.org/ontology/JERMOntology#>
    PREFIX schema: <http://schema.org/>
    SELECT DISTINCT ?id ?predicate ?object
    WHERE {
        ?assay ?predicate ?object .
        ?assay a unlock:AmpliconAssay .
        ?assay schema:identifier ?id .
        FILTER(!ISIRI(?object))
    }""")

    print(len(qres))
    for index, row in enumerate(qres):
        print(index, end="\r")
        # print(row)
        predicate = "assay_"+row.predicate.split("/")[-1]
        identifier = row.id
        obj = str(row.object)
        if "\t" in obj:
            print(">>>>" + obj)
        
        print(identifier + "\t" + predicate + "\t" + obj, file=output)

    qres = g.query("""
    PREFIX gbol:<http://gbol.life/0.1/>
    PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
    PREFIX ssb: <http://ssb.wur.nl/>
    PREFIX ns1: <http://ssb.wur.nl/model/>
    PREFIX unlock: <http://m-unlock.nl/ontology/>
    PREFIX jerm: <http://jermontology.org/ontology/JERMOntology#>
    PREFIX schema: <http://schema.org/>
    SELECT DISTINCT ?id ?predicate ?object ?predicate_label
    WHERE {
        ?sample a jerm:Sample .
        ?sample ?predicate ?object .
        OPTIONAL { ?predicate rdfs:label ?predicate_label}
        ?sample jerm:hasPart ?assay .
        ?assay a unlock:AmpliconAssay .
        ?assay schema:identifier ?id .
        FILTER(!ISIRI(?object))
    }""")
    # Flush to be sure?
    output.flush()
    print(len(qres))
    for index, row in enumerate(qres):
        print(index, end="\r")
        predicate = "sample_" + row.predicate.split("/")[-1]
        if type(row.predicate_label) == Literal:
            predicate = "sample_" + row.predicate_label.replace(" ","_")
        identifier = row.id
        obj = row.object
        if "\t" in obj:
            print(">>>>" + obj)
        print(identifier + "\t" + predicate + "\t" + str(obj), file=output)
    
    output.close()   

# def process_rdf_files_test(rdf_file):
#     g = Graph()
#     g.parse(rdf_file, format="turtle")
#     query = """
#     PREFIX unlock: <http://m-unlock.nl/ontology/>
#     PREFIX schema: <http://schema.org/>
#     SELECT DISTINCT ?id ?predicate ?object
#     WHERE {
#         ?assay a jerm:Assay .
#         ?assay schema:additionalType unlock:Amplicon .
#         ?assay schema:identifier ?id .
#         ?assay ?predicate ?object .
#         FILTER(!ISIRI(?object))
#     }"""
#     print(rdf_file)

#     qres = g.query(query)
#     print(qres.__dict__)
#     for row in qres:
#         print(row)

# def process_rdf_files_roqet(rdf_file):
#     # roqet -D /unlock/projects/PRJ_E2BN/INV_LORA/LORA_unlock_2021_excl_samples.ttl -r tsv query.sparql
#     print("Processing rdf file", rdf_file)
#     # g = Graph()
#     # g.bind("unlock", Namespace("http://m-unlock.nl/ontology/"))
#     # g.bind("schema", Namespace("http://schema.org/"))
#     # g.parse(rdf_file, format="turtle")
#     # output = open("metadata.picrust.tsv", "a")
#     query = """
#     PREFIX unlock: <http://m-unlock.nl/ontology/>
#     PREFIX schema: <http://schema.org/>
#     SELECT DISTINCT ?id ?predicate ?object
#     WHERE {
#         ?assay a jerm:Assay .
#         ?assay schema:additionalType unlock:Amplicon .
#         ?assay schema:identifier ?id .
#         ?assay ?predicate ?object .
#         FILTER(!ISIRI(?object))
#     }"""

#     query_file = open("query.sparql", "w")
#     print(query, file=query_file)
#     query_file.close()
#     command = "roqet -D "+rdf_file+" -r tsv query.sparql > query.results"
#     os.system(command)

#     # for row in qres:
#     #     predicate = "assay_"+row.predicate.split("/")[-1]
#     #     identifier = row.id
#     #     obj = row.object
#     #     if "\t" in obj:
#     #         print(">>>>" + obj)
#     #     print(identifier + "\t" + predicate + "\t" + obj, file=output)
#     # Flush to be sure?
#     # output.flush()

#     query = """
#     PREFIX unlock: <http://m-unlock.nl/ontology/>
#     PREFIX jerm: <http://jermontology.org/ontology/JERMOntology#>
#     PREFIX schema: <http://schema.org/>
#     PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
#     SELECT DISTINCT ?id ?predicate ?object ?predicate_label
#     WHERE {
#         ?sample a jerm:Sample .
#         ?sample jerm:hasPart ?assay .
#         ?assay a unlock:Assay .
#         ?assay schema:additionalType unlock:Amplicon .
#         ?assay schema:identifier ?id .
#         ?sample ?predicate ?object .
#         OPTIONAL { ?predicate rdfs:label ?predicate_label}
#         FILTER(!ISIRI(?object))
#     }"""
#     query_file = open("query.sparql", "w")
#     print(query, file=query_file)
#     query_file.close()
#     command = "roqet -D "+rdf_file+" -r tsv query.sparql >> query.results"
#     os.system(command)

#     # for row in qres:
#     #     predicate = "sample_" + row.predicate.split("/")[-1]
#     #     if type(row.predicate_label) == Literal:
#     #         predicate = "sample_" + row.predicate_label.replace(" ","_")
#     #     identifier = row.id
#     #     obj = row.object
#     #     if "\t" in obj:
#     #         print(">>>>" + obj)
#     #     print(identifier + "\t" + predicate + "\t" + obj, file=output)
    
#     # output.close()   
        
        



def tsv_to_biom(input_file):
    # Formatting
    try:
        result = pd.read_table(input_file)
    except pd.errors.EmptyDataError:
        print("No content for", input_file)
        return
    result.fillna(0, inplace=True)
    result = result.round(0)
    result.to_csv(input_file, sep="\t")
    # Transform to biom tsv format
    lines = open(input_file).readlines()
    lines.insert(0, "# Automatically generated input file for biom conversion")
    lines[1] = "#OTU ID\t" + lines[1]
    output = open(input_file, "w")
    for line in lines:
        print(line.strip(), file=output)

    biom_content = load_table(input_file)
    
    # Load the metadata file
    metadata = pd.read_table("metadata.picrust.tsv").to_dict()
    for key in metadata:
        for key2 in metadata[key]:
            metadata[key][key2] = str(metadata[key][key2])

    # Add metadata
    biom_content.add_metadata(metadata)
    biom_content.type = "Function table"
    json_data = biom_content.to_json(generated_by="UNLOCK conversion module")
    
    # Create Python object from JSON string data
    obj = json.loads(json_data)
    
    # TODO find out where this 0 element comes from (pandas?)
    obj['columns'] = obj['columns'][1:]
    # for row in obj['rows']:
        # row['metadata'] = {"taxonomy":["k__NA", "p__NA", "c__NA", "o__NA", "f__NA", "g__NA", "s__NA"]}
    obj['shape'][1] = int(obj['shape'][1]) - 1
    # Pretty Print JSON
    json_formatted_str = json.dumps(obj, indent=4, sort_keys=True)
    
    biom_file = args.identifier + "_" + input_file.replace(".tsv", ".biom")
    print("Writing biom file to", biom_file)
    print(json_formatted_str, file=open(biom_file, "w"))


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Biom file creation')
    parser.add_argument('-j', '--job', help='input Job file', required=True)
    parser.add_argument('-i', '--identifier', help='Prefix identifier', required=True)

    args = parser.parse_args()

    job = args.job

    if os.path.isfile("metadata.picrust.tsv"):
        os.remove("metadata.picrust.tsv")

    # Process the job file to create a biom json file
    process_job_file()
