#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Unlock, Heroes Assembled...

Description:
Add author info to the end of a cwl script (commandlinetool or workflow).
You can input a yaml file for the author info, or input the author info directly by answering the questions prompted. 
Typical author info incl name, orcid, email, they are like:

```yaml
s:author:
  - class: s:Person
    s:identifier: https://orcid.org/0000-0001-8172-8981
    s:email: mailto:jasper.koehorst@wur.nl
    s:name: Jasper Koehorst
  - class: s:Person
    s:identifier: https://orcid.org/0000-0001-9524-5964
    s:email: mailto:bart.nijsse@wur.nl
    s:name: Bart Nijsse
  - class: s:Person
    s:identifier: https://orcid.org/0009-0001-1350-5644
    s:email: mailto:changlin.ke@wur.nl
    s:name: Changlin Ke
```

If below metadata is (or partly) not present in the cwl script, add the following metadata (configurable) to ~the end of~ the cwl script as well. 

```yaml
s:citation: https://m-unlock.nl
s:codeRepository: https://gitlab.com/m-unlock/cwl
s:dateCreated: "2022-09-00"
s:dateModified: "2024-01-14"
s:license: https://spdx.org/licenses/Apache-2.0
s:copyrightHolder: "UNLOCK - Unlocking Microbial Potential"
```

Usage:
python cwl2bib.py [-h] [-y YAML_FILE] [-p] input_path output_path
input and output path can be a file or a directory, if it is a directory, all cwl files in the directory will be recursively processed and saved in same file sctructure.

Author:
Changlin Ke (chagnlin.ke@wur.nl)

Date:
2024-10-04
"""

import os
import yaml
from ruamel.yaml import YAML
from ruamel.yaml.parser import ParserError
import argparse
from dataclasses import dataclass, field
from typing import List, Optional, Dict
from pathlib import Path
from datetime import datetime
import shutil


# Store author information as a list of dictionaries directly in the AuthorInfo class
@dataclass
class AuthorInfo:
    authors: List[dict] = field(default_factory=list)

    def load_from_dict(self, data: dict):
        self.authors = data.get('s:author', [])

    def prompt_for_authors(self):
        # Manually input author information
        while True:
            name = input("Enter author name (or 'q' to quit): ")
            if name == 'q':
                break
            orcid = input(f"Enter ORCID for {name}: ")
            email = input(f"Enter email for {name}: ")
            self.authors.append({
                'class': 's:Person',
                's:name': name,
                's:identifier': orcid,
                's:email': email
            })
    
    def get_existing_authors(self):
        return self.authors

    def to_text(self, existing_authors):
        # Convert author information to text format, only add authors that do not already exist
        author_text = ""
        for author in self.authors:
            if any(existing_author.get('s:identifier') == author['s:identifier'] for existing_author in existing_authors):
                continue  # Skip authors that already exist
            author_text += f"  - class: s:Person\n"
            author_text += f"    s:identifier: {author['s:identifier']}\n"
            author_text += f"    s:email: {author['s:email']}\n"
            author_text += f"    s:name: {author['s:name']}\n"
        return author_text


# Dataclass for Metadata
@dataclass
class Metadata:
    citation: Optional[str] = None
    coderepository: Optional[str] = None
    datecreated: Optional[str] = None
    datemodified: Optional[str] = None
    license: Optional[str] = None
    copyrightholder: Optional[str] = None

    def load_from_dict(self, data: dict):
        metadata_data = {k: v for k, v in data.items() if k.startswith('s:') and not k.startswith('s:author')}
        for key, value in metadata_data.items():
            setattr(self, key.replace('s:', '').lower(), value)

    def get_existing_metadata(self):
        # Return a dictionary of the current metadata
        metadata_dict = {
            's:citation': self.citation,
            's:codeRepository': self.coderepository,
            's:dateCreated': self.datecreated,
            's:dateModified': self.datemodified,
            's:license': self.license,
            's:copyrightHolder': self.copyrightholder
        }
        # Filter out empty values
        return {k: v for k, v in metadata_dict.items() if v is not None}

    def to_text(self, existing_metadata):
        # Convert metadata to text format, only add metadata items that do not already exist
        metadata_text = ""

        # Ensure that if dateModified is not provided, use the current date
        if self.datemodified is None:
            self.datemodified = datetime.now().strftime('%Y-%m-%d')
        
        metadata_fields = {
            's:citation': self.citation,
            's:codeRepository': self.coderepository,
            's:dateCreated': self.datecreated,
            's:dateModified': self.datemodified,
            's:license': self.license,
            's:copyrightHolder': self.copyrightholder
        }

        for key, value in metadata_fields.items():
            if key in existing_metadata:
                continue  # Skip metadata that already exists
            metadata_text += f"{key}: {value}\n"
        return metadata_text



# CWL Processor class to handle file and folder operations
class CWLProcessor:
    def __init__(self, author_info: AuthorInfo, metadata: Metadata):
        self.author_info = author_info
        self.metadata = metadata

    def read_existing_content(self, input_file):
        """Read existing YAML content and extract existing author information and metadata"""
        with open(input_file, 'r') as f:
            lines = f.readlines()

        # Use load_yaml to read the input file
        content = load_yaml(input_file)

        # Initialize Metadata and AuthorInfo instances
        existing_metadata = Metadata()
        existing_metadata.load_from_dict(content)  # Load metadata from content

        existing_authors = AuthorInfo()
        existing_authors.load_from_dict(content)  # Load author information from content

        print(f"Existing authors: {existing_authors}", "\n", f"Existing metadata: {existing_metadata}")
        return lines, existing_metadata, existing_authors

    def append_to_file(self, input_file, output_file):
        # Read existing file content and extract existing author information and metadata
        original_lines, existing_metadata_instance, existing_authors_instance = self.read_existing_content(input_file)
        # Get the list of existing authors
        existing_authors = existing_authors_instance.get_existing_authors()
        existing_metadata = existing_metadata_instance.get_existing_metadata()
        # Create a dictionary to store the content to be inserted into the lines
        lines_to_insert = {}

        # If author information exists, only add authors that do not already exist
        if self.author_info.authors:
            additional_authors = self.author_info.to_text(existing_authors)
            print(f"To add author:\n {additional_authors}")
            if additional_authors:
                # Find the position of the author information or choose an appropriate insertion point
                author_insert_pos = next(
                    (i for i, line in enumerate(original_lines) if line.strip() == 's:author:'),
                    len(original_lines) # if not found, insert at the end
                )
                lines_to_insert[author_insert_pos] = additional_authors

        # Only add metadata that does not already exist
        additional_metadata = self.metadata.to_text(existing_metadata)
        if additional_metadata:
            print(f"To add metadata:\n {additional_metadata}")
            # Find the position of the metadata information or choose an appropriate insertion point
            metadata_insert_pos = next(
                (i for i, line in enumerate(original_lines) if line.startswith('s:citation')),
                len(original_lines)
            )
            lines_to_insert[metadata_insert_pos + 1] = additional_metadata

        # Create a copy of the source file
        shutil.copy(input_file, output_file)

        # Write the content to the copy according to the insertion points
        with open(output_file, 'r+') as f:
            lines = f.readlines()  # Read the content of the copy
            f.seek(0)  # Move the file pointer to the beginning of the file
            for i, line in enumerate(lines):
                f.write(line)
                if i in lines_to_insert:
                    f.write(lines_to_insert[i])
            f.truncate()  

        print(f"Processed: {input_file} -> {output_file}")

    def process_directory(self, input_dir, output_dir):
        input_dir = Path(input_dir)
        output_dir = Path(output_dir)

        for cwl_file in input_dir.rglob('*.cwl'):
            relative_path = cwl_file.relative_to(input_dir)
            output_path = output_dir / relative_path
            output_path.parent.mkdir(parents=True, exist_ok=True)
            self.append_to_file(cwl_file, output_path)

    def process(self, input_path, output_path):
        if Path(input_path).is_file():
            self.append_to_file(input_path, output_path)
        elif Path(input_path).is_dir():
            self.process_directory(input_path, output_path)
        else:
            print(f"Error: {input_path} is not a valid file or directory")



def load_yaml(file_path: str):
    """Read YAML file using ruamel.yaml and return the data structure"""
    yaml = YAML()
    with open(file_path, 'r') as f:
        data = yaml.load(f)
        print(f"Loaded YAML data from {file_path}")
    return data



def parse_args(ascii_art):
    parser = argparse.ArgumentParser(
        description=f"{ascii_art} \r Add author and metadata to CWL scripts.",
        formatter_class=argparse.RawDescriptionHelpFormatter # important to keep ascii art formatting
    )
    parser.add_argument(
        "input_path", type=str,
        help="Path to the input CWL script or folder containing CWL scripts"
    )
    parser.add_argument(
        "output_path", type=str,
        help="Path to the output CWL script or folder for saving the updated CWL scripts"
    )
    parser.add_argument(
        "-y", "--yaml_file", type=str, default=None,
        help="Optional YAML file containing author and metadata information"
    )
    parser.add_argument(
        "-p", "--prompt", action="store_true",
        help="Prompt for author information if no YAML file is provided"
    )
    return parser.parse_args()



if __name__ == "__main__":
    
    ascii_art = """
  _   _ _   _ _     ___   ____ _  __     ____  ____  _____ ____  _____ _   _ _____ ____  
 | | | | \ | | |   / _ \ / ___| |/ /    |  _ \|  _ \| ____/ ___|| ____| \ | |_   _/ ___| 
 | | | |  \| | |  | | | | |   | ' /     | |_) | |_) |  _| \___ \|  _| |  \| | | | \___ \ 
 | |_| | |\  | |__| |_| | |___| . \     |  __/|  _ <| |___ ___) | |___| |\  | | |  ___) |
  \___/|_| \_|_____\___/ \____|_|\_\    |_|   |_| \_\_____|____/|_____|_| \_| |_| |____/ 
                                                                                         
    """
    args = parse_args(ascii_art)
    print(ascii_art, "\n")

    if args.yaml_file:
        yaml_data = load_yaml(args.yaml_file)

        # Init AuthorInfo and Metadata
        author_info = AuthorInfo()
        author_info.load_from_dict(yaml_data)

        metadata = Metadata()
        metadata.load_from_dict(yaml_data)
    elif args.prompt:
        # Prompt for author information if no YAML file is provided
        author_info = AuthorInfo()
        author_info.prompt_for_authors()

        metadata = Metadata()
    else:
        print("Error: You must provide either a YAML file or use the --prompt option to input author information.")
        exit(1)

    processor = CWLProcessor(author_info, metadata)

    # Process the input file or directory
    processor.process(args.input_path, args.output_path)

    print("All Done!")